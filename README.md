---
home: true
#heroImage: /hero.jpeg
actionLink: Figures
actionText: "See figures"
heroText: "Polarization Near Dislocation Cores in SrTiO3 Single Crystals: The Role of Flexoelectricity"
tagline: Xiaoxing Cheng
lang: en-US
sidebar: auto
---
    