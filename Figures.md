---
title: "Polarization Near Dislocation Cores in SrTiO3 Single Crystals: The Role of Flexoelectricity"
lang: en-US
sidebar: auto
---
    

## Figure 1
![Figure 1](figure1.svg)

Stress and strain gradient distribution around dislocation core. 
(a) stress11, (b) stress33, (c) stress13, 
(d) strain 11 gradient, (e) strain 33 gradient, (f) strain 13 gradient.



## Figure 2
![Figure 2](figure2.svg)

Comparison of polarization distribution with and without flexoelectric effect.
(a) polarization distribution without flexoelectricity, 
(b) polarization distribution considering flexoelectricity,
The quivers in (a),(b) indicates the polarization vector and the background heat plot illustrate the magnitude of polarization.
(c) Px, Pz and P magnitude statistics for (a) and (b).
The upper panel is the result for aberage polarization.
The lower panel is the result for maximum polarization within the same region.
Each bar is an average value of 5 calculations and the standard deviation is shown by the black line on bar top.
(d) and (e) Flexoelectric field distribution, quivers indicate the flexoelectric field and background heat plot shows the x and z component of flexoelectric field respectively.
(f) polarization mapping of 10 degrees SrTiO3 bicrystal, the direction of polarization is indicated by the arrows.
The white trapezoid represent the dislocation core.



## Figure 3
![Figure 3](figure3.svg)

The polarization and flexoelectric field distribution under different flexoelectric coefficients for (100) dislocation.
White quiver represent the plotted vector field, and background heat plot shows the magnitude of the vector.
(a), (b) and (c) polarization distribution.
(d), (e) and (f) flexoelectric field distribution.
(a, d) non-zero longitudinal coefficient.
(b, e) non-zero transverse coefficient.
(c, f) non-zero shear coefficient.



## Figure 4
![Figure 4](figure4.svg)

The polarization distribution with different flexoelectric coefficients for (110) dislocation.
(a)no-flexo. 
(b) experimental flexoelectric coefficient 
(d) non-zero longitudinal coefficient 
(e) non-zero transversecoefficient 
(f) non-zero shear coefficient. 
(c) Sum of total polarization for each orientation angle with respectto the (100) direction. 
Peak means more polarization is aligned along such direction.



## Supplementary 1
![Supplementary 1](sup1.svg)

The simulation setup.
(a) a total of 80 jobs, with 8 sets of flexoelectric coefficients, 2 sets of burgers vector, and 5 different random seeds were calculated.
(b) (100) dislocation setup,
(c) (110) dislocation setup.
The plotted value is sigma11 for the purpose of showing location of the dislocations.



## Supplementary 2
![Supplementary 2](sup2.svg)

Comparison of stress distribution around (100) dislocation core between analytical and simulation results. 
(a), (b), and (c) analytical stress distribution, compariable with Figure 1.
(d), (e), and (f) are line profile comparison along the green line in (a), (b), and (c) correspondingly. 
Dislocation locates at the center, which is labelled by the green T marker.


## Supplementary 3
![Supplementary 3](sup3.svg)

Stress and strain gradient distribution around (110) dislocation core. 


## Supplementary 4
![Supplementary 4](sup4.svg)

Illustration of each strain gradient componentís contribution to the final flexoelectric field for case8. 
The light green column is strain gradient, the area of each circle is proportional to the maximum value ofthat strain gradient component. 
The dark green column is the flexoelectric coefficient for case 8, the area ofwhich is proportional to the flexoelectric coefficent value.
The two rectangles in the right column represent thex and z component of flexoelectric field vector. 
The area of each color shows the contribution from each straingradient.


## Supplementary 5
![Supplementary 5](sup5.svg)

The polarization and flexoelectric field distribution with different flexoelectric coefficients (case 6, 7, 8) for (100) dislocation.



## Supplementary 6
![Supplementary 6](sup6.svg)

Polarization and flexoelectric field distribution of case 6, 7, 8 for (110) dislocation.



## Supplementary 7
![Supplementary 7](sup7.svg)

Polarization and flexoelectric field distribution of case 3, 4, 5 for (110) dislocation.



## Supplementary 8
![Supplementary 8](sup8.svg)

Stress and strain gradient distribution around (110) dislocation core. 
The dimension of the plottedregion is 10nm by 10nm, and the dislocation is located at the center, labeled by the T shape marker.


## Supplementary 9
![Supplementary 9](sup9.png)

Stress and strain gradient distribution around (110) dislocation core. 
The dimension of the plottedregion is 10nm by 10nm, and the dislocation is located at the center, labeled by the T shape marker.
